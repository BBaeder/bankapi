import { ClientDAO } from "../src/daos/client-dao";
import { ClientDAOImpl } from "../src/daos/client-dao-impl";
import { Client } from "../src/entities";

const clientDAO:ClientDAO = new ClientDAOImpl();

const testClient:Client = new Client(0, "Brystan", "Baeder");

test("Create a client, changed ID", async ()=>{
    const result:Client = await clientDAO.createClient(testClient);
    expect(result.primaryKey).not.toBe(0);
})
test("Get client by ID", async ()=>{
    let client:Client = new Client(0,'Dracula','Stoker');
    client = await clientDAO.createClient(client);
    let retrievedClient:Client = await clientDAO.getClientByPrimaryKey(client.primaryKey);
    expect(retrievedClient.primaryKey).toBe(client.primaryKey);
});
test("Get all clients", async ()=>{
    let client1:Client = new Client(0,'Sapiens','Yuval');
    let client2:Client = new Client(0,'Josh','Orwell');
    let client3:Client = new Client(0,'Paradox','Barry');
    await clientDAO.createClient(client1);
    await clientDAO.createClient(client2);
    await clientDAO.createClient(client3);
    const clients:Client[]= await clientDAO.getAllClients();
    expect(clients.length).toBeGreaterThanOrEqual(3);
});

test("Update Client", async ()=>{
    let client:Client = new Client(0,'Castle','Jackson');
    client = await clientDAO.createClient(client);
    client.lname = "Updated"
    client = await clientDAO.updateClient(client);
    expect(client.lname).toBe("Updated");
});

test("Delete Client", async ()=>{
    let client:Client = new Client(0,'Frank','Mary');
    client = await clientDAO.createClient(client);
    const result:boolean = await clientDAO.deleteClientByPrimaryKey(client.primaryKey);
    expect(result).toBeTruthy();
});