import { AccountDAO } from "../src/daos/account-dao";
import { AccountDAOImpl } from "../src/daos/account-dao-impl";
import { ClientDAO } from "../src/daos/client-dao";
import { ClientDAOImpl } from "../src/daos/client-dao-impl";
import { Account, Client } from "../src/entities";

const accountDAO:AccountDAO = new AccountDAOImpl();
const clientDAO:ClientDAO = new ClientDAOImpl();

test("Create an account, changed primary key", async ()=>{
    const temp:Client = new Client(0,"Bry","Bae");
    const tc = await clientDAO.createClient(temp);
    const testAcc = new Account(0,true,1000,tc.primaryKey);
    const result:Account = await accountDAO.createAccount(testAcc, tc.primaryKey);
    expect(result.primaryKey).not.toBe(0);
})

test("Create an account, foreign key same as attached client", async ()=>{
    const temp:Client = new Client(0,"Joe","Schmoe");
    const tc = await clientDAO.createClient(temp);
    const testAcc = new Account(0,true,100,tc.primaryKey);
    const result:Account = await accountDAO.createAccount(testAcc, tc.primaryKey);
    expect(result.foreignKey).toBe(tc.primaryKey);
})

test("Get account by primary key", async ()=>{
    let account:Account = new Account(0,true,1500,1);
    account = await accountDAO.createAccount(account, account.foreignKey);
    let retrievedAccount:Account = await accountDAO.getAccountByPrimaryKey(account.primaryKey);
    expect(retrievedAccount.primaryKey).toBe(account.primaryKey);
});

test("Get all accounts by foreign key", async ()=>{
    let account:Account = new Account(0,true,2100,1);
    account = await accountDAO.createAccount(account, account.foreignKey);
    let retrievedAccounts:Account[] = await accountDAO.getAllAccountsByForeignKey(account.foreignKey);
    expect(retrievedAccounts[0].foreignKey).toBe(account.foreignKey);
});

test("Get all accounts", async ()=>{
    const temp1:Client = new Client(0,"Joe","Schmoe");
    const tc = await clientDAO.createClient(temp1);
    let acc1:Account = new Account(0,false,0,1);
    let acc2:Account = new Account(0,true,0,2);
    await accountDAO.createAccount(acc1, acc1.foreignKey);
    await accountDAO.createAccount(acc2, acc2.foreignKey);
    const accounts:Account[]= await accountDAO.getAllAccounts();
    expect(accounts.length).toBeGreaterThanOrEqual(2);
});

test("Update Account, amount correct", async ()=>{
    let account:Account = new Account(0,true,0,1);
    account = await accountDAO.createAccount(account,account.foreignKey);
    account.amount = 250
    account = await accountDAO.updateAccount(account);
    expect(account.amount).toBe(250);
});

test("Update Account, type", async ()=>{
    let account:Account = new Account(0,true,0,2);
    account = await accountDAO.createAccount(account,account.foreignKey);
    account.isChecking = false;
    account = await accountDAO.updateAccount(account);
    expect(account.isChecking).toBe(false);
});

test("Delete Account by primary key", async ()=>{
    let account:Account = new Account(0,false,0,1);
    account = await accountDAO.createAccount(account, account.foreignKey);
    const result:boolean = await accountDAO.deleteAccountByPrimaryKey(account.primaryKey);
    expect(result).toBeTruthy();
});

test("Delete all Accounts by foreign key", async ()=>{
    const temp3:Client = new Client(0,"Delete","Accounts");
    const testClient3 = await clientDAO.createClient(temp3);
    const acc1 = new Account(0,true,500,testClient3.primaryKey);
    const acc2 = new Account(0,false,500,testClient3.primaryKey);
    const acc3 = new Account(0,true,500,testClient3.primaryKey);
    await accountDAO.createAccount(acc1, acc1.foreignKey);
    await accountDAO.createAccount(acc2, acc2.foreignKey);
    await accountDAO.createAccount(acc3, acc3.foreignKey);
    const result:boolean = await accountDAO.deleteAllAccountsByForeignKey(testClient3.primaryKey);
    expect(result).toBeTruthy();
});