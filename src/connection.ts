import { Client } from 'pg';

export const connect = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD,
    database:'bankdb',
    port:5432,
    host:'34.150.195.205'
})
connect.connect();