import express from 'express';
import { Account, Client } from './entities';
import { InsufficientFunds, MissingResourceError } from './errors';
import AccountService from './services/account-service';
import { AccountServiceImpl } from './services/account-service-impl';
import ClientService from './services/client-service';
import { ClientServiceImpl } from './services/client-service-impl';

const app = express();
app.use(express.json());

const clientService:ClientService = new ClientServiceImpl();
const accountService:AccountService = new AccountServiceImpl();

app.get("/clients", async (req, res)=>{
    try {
        const clients:Client[] = await clientService.retrieveAllClients();
        res.status(200);
        res.send(clients);
    } catch (error) {
        res.status(404);
        res.send(error);
    }

})
app.post("/clients", async (req, res)=>{
    let client:Client = req.body;
    client = await clientService.registerClient(client);
    res.status(201);
    res.send(client);
})
app.get("/clients/:id", async (req, res)=>{
    try {
        const id:number = Number(req.params.id);
        const client:Client = await clientService.retrieveClientByPrimaryKey(id);
        res.send(client);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.put("/clients/:id", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        let client:Client = req.body;
        client.primaryKey=id
        client = await clientService.modifyClient(client);
        res.send(client);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.delete("/clients/:id", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        const ret = await clientService.removeClientByPrimaryKey(id);
        res.send(`The client with ID ${id} as well as all associated accounts has been removed.`)
        res.status(205);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.post("/clients/:id/accounts", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        const client = await clientService.retrieveClientByPrimaryKey(id);
        let account:Account = req.body
        account = await accountService.registerAccount(account, id)
        res.send(account);
        res.send(201)
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.get("/clients/:id/accounts", async (req, res)=>{
    try {
        const id:number = Number(req.params.id);
        const accounts:Account[] = await accountService.retrieveAllAccountsByForeignKey(id);
        res.send(accounts);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.get("/accounts", async (req, res)=>{
    try{
        const accounts:Account[] = await accountService.retrieveAllAccounts();
        res.status(200);
        res.send(accounts);
    } catch (error) {
        res.status(404);
        res.send(error);
    }
})
app.get("/accounts/:id", async (req, res)=>{
    try {
        const id:number = Number(req.params.id);
        const account:Account = await accountService.retrieveAccountByPrimaryKey(id);
        res.send(account);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.put("/accounts/:id", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        let account:Account = req.body;
        account.primaryKey = id
        account = await accountService.modifyAccount(account);
        res.send(account);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.delete("/accounts/:id", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        const ret = await accountService.removeAccountByPrimaryKey(id);
        res.send(`The account with ID ${id} has been removed.`)
        res.status(205);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.patch("/accounts/:id/deposit", async (req, res)=>{
    try {
        const id:number = Number(req.params.id);
        const deposit = req.body;
        let account:Account = await accountService.retrieveAccountByPrimaryKey(id);
        const currAm = Number(account.amount);
        const newAm = currAm + Number(deposit.amount);
        account.amount = newAm;
        account = await accountService.modifyAccount(account);
        res.send(account)
    } catch (error) {
        res.status(404);
        res.send(error);
    }
})
app.patch("/accounts/:id/withdraw", async (req, res)=>{
    try {
        const id:number = Number(req.params.id);
        const withdraw = req.body;
        let account:Account = await accountService.retrieveAccountByPrimaryKey(id);
        const currAm = Number(account.amount);
        const newAm = currAm - Number(withdraw.amount);
        account.amount = newAm;
        account = await accountService.modifyAccount(account);
        res.send(account)
    } catch (error) {
        if (error instanceof InsufficientFunds){
            res.status(422);
            res.send(error)

        } else {
            res.status(404);
            res.send(error);
        }
    }
})
app.listen(3000,()=>{console.log("Application Started")});