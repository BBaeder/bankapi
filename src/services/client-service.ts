import { Client } from "../entities";

export default interface ClientService{

    registerClient(client:Client):Promise<Client>;

    retrieveAllClients():Promise<Client[]>;

    retrieveClientByPrimaryKey(primaryKey:number):Promise<Client>;

    SearchByFirstName(firstName:string):Promise<Client[]>;

    SearchByLastName(lastName:string):Promise<Client[]>;

    modifyClient(client:Client):Promise<Client>;

    removeClientByPrimaryKey(primaryKey:number):Promise<boolean>;
    
}