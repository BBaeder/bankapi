import { Account } from "../entities";

export default interface AccountService{

    registerAccount(account:Account, foreignKey:number):Promise<Account>;

    retrieveAllAccounts():Promise<Account[]>;

    retrieveAccountByPrimaryKey(primaryKey:number):Promise<Account>;

    retrieveAllAccountsByForeignKey(foreignKey:number):Promise<Account[]>;

    modifyAccount(account:Account):Promise<Account>;

    removeAccountByPrimaryKey(primaryKey:number):Promise<boolean>;

    removeAllAccountsByForeignKey(foreignKey:number):Promise<boolean>;
    
}