import { AccountDAO } from "../daos/account-dao";
import { AccountDAOImpl } from "../daos/account-dao-impl";
import { ClientDAO } from "../daos/client-dao";
import { ClientDAOImpl } from "../daos/client-dao-impl";
import { Account } from "../entities";
import { InsufficientFunds, MissingResourceError } from "../errors";
import AccountService from "./account-service";

export class AccountServiceImpl implements AccountService{

    accountDAO:AccountDAO = new AccountDAOImpl();
    clientDAO:ClientDAO = new ClientDAOImpl();

    registerAccount(account: Account, foreignKey:number): Promise<Account> {
        try {
            this.clientDAO.getClientByPrimaryKey(foreignKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find client with ID ${foreignKey}`);
        }
        return this.accountDAO.createAccount(account, foreignKey);
    }

    retrieveAllAccounts(): Promise<Account[]> {
        return this.accountDAO.getAllAccounts();
    }
    retrieveAccountByPrimaryKey(primaryKey: number): Promise<Account> {
        try {
            return this.accountDAO.getAccountByPrimaryKey(primaryKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find account with ID ${primaryKey}`);
        }
    }
    retrieveAllAccountsByForeignKey(foreignKey: number): Promise<Account[]> {
        try {
            this.clientDAO.getClientByPrimaryKey(foreignKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find client with ID ${foreignKey}`);
        }
        return this.accountDAO.getAllAccountsByForeignKey(foreignKey);
    }

    modifyAccount(account: Account): Promise<Account> {
        if (account.amount < 0){
            throw new InsufficientFunds("Not enough money in account to withdraw that much.");
        }
        try{
            this.accountDAO.getAccountByPrimaryKey(account.primaryKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find account with ID ${account.primaryKey}`);
        }
        return this.accountDAO.updateAccount(account);
    }

    removeAccountByPrimaryKey(primaryKey: number): Promise<boolean> {
        try{
            this.accountDAO.getAccountByPrimaryKey(primaryKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find account with ID ${primaryKey}`);
        }
        return this.accountDAO.deleteAccountByPrimaryKey(primaryKey);
    }
    
    async removeAllAccountsByForeignKey(foreignKey: number): Promise<boolean> {
        try {
            this.clientDAO.getClientByPrimaryKey(foreignKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find client with ID ${foreignKey}`);
        }
        return this.accountDAO.deleteAllAccountsByForeignKey(foreignKey);
    }

}