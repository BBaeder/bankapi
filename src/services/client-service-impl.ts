import { AccountDAO } from "../daos/account-dao";
import { AccountDAOImpl } from "../daos/account-dao-impl";
import { ClientDAO } from "../daos/client-dao";
import { ClientDAOImpl } from "../daos/client-dao-impl";
import { Client } from "../entities";
import { MissingResourceError } from "../errors";
import ClientService from "./client-service";

export class ClientServiceImpl implements ClientService{

    clientDAO:ClientDAO = new ClientDAOImpl();
    accountDAO:AccountDAO = new AccountDAOImpl();

    registerClient(client: Client): Promise<Client> {
        return this.clientDAO.createClient(client);
    }

    retrieveAllClients(): Promise<Client[]> {
        return this.clientDAO.getAllClients();
    }

    retrieveClientByPrimaryKey(primaryKey: number): Promise<Client> {
        try {
            return this.clientDAO.getClientByPrimaryKey(primaryKey);
        } catch (error) {
            throw new MissingResourceError(`Could not find client with ID ${primaryKey}`);
        }
    }

    async SearchByFirstName(firstName: string): Promise<Client[]> {
        let clients:Client[] = await this.clientDAO.getAllClients();
        let gotten = [];
        for(let i = 0; i < clients.length; i++){
            if(clients[i].fname === firstName){
                gotten.push(clients[i]);
            }
        }
        return gotten;
    }

    async SearchByLastName(lastName: string): Promise<Client[]> {
        let clients:Client[] = await this.clientDAO.getAllClients();
        let gotten = [];
        for(let i = 0; i < clients.length; i++){
            if(clients[i].lname === lastName){
                gotten.push(clients[i]);
            }
        }
        return gotten;
    }

    modifyClient(client: Client): Promise<Client> {
        try{
            this.clientDAO.getClientByPrimaryKey(client.primaryKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find client with ID ${client.primaryKey}`);
        }
        return this.clientDAO.updateClient(client);
    }

    removeClientByPrimaryKey(primaryKey: number): Promise<boolean> {
        this.accountDAO.deleteAllAccountsByForeignKey(primaryKey);
        return this.clientDAO.deleteClientByPrimaryKey(primaryKey);
    }
}