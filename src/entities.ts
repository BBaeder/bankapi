export class Client{
    constructor(
        public primaryKey:number,
        public fname:string="",
        public lname:string=""
    ){}}
export class Account{
    constructor(
        public primaryKey:number,
        public isChecking:boolean,
        public amount:number=0,
        public foreignKey:number
    ){}
}