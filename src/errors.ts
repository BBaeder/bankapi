export class MissingResourceError{
    message:string;
    description:string = "Resource could not be found";
    constructor(message:string){
        this.message = message;
    }
}
export class InsufficientFunds{
    message:string;
    description:string = "Insufficient funds for withdraw";
    constructor(message:string){
        this.message=message;
    }
}