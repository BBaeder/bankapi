import { Client } from "../entities";
import { ClientDAO } from "./client-dao";
import { connect } from "../connection";

export class ClientDAOImpl implements ClientDAO {
    async createClient(client: Client): Promise<Client> {
        const sql:string = 'insert into client(first_name,last_name) values ($1,$2) returning primary_key';
        const values = [
            client.fname,
            client.lname
        ];
        const result = await connect.query(sql, values);
        client.primaryKey = result.rows[0].primary_key;
        return client;
    }
    async getAllClients():Promise<Client[]> {
        const sql:string = "select * from client";
        const result = await connect.query(sql);
        const clients:Client[] = [];
        for(const row of result.rows){
            const client:Client = new Client(
                row.primary_key,
                row.first_name,
                row.last_name);
            clients.push(client);
        }
        return clients;
    }
    async getClientByPrimaryKey(primaryKey: number): Promise<Client> {
        const sql:string = 'select * from client where primary_key = $1';
        const values = [primaryKey];
        const result = await connect.query(sql, values);
        const row = result.rows[0];
        const client:Client = new Client(
            row.primary_key,
            row.first_name,
            row.last_name);
        return client;
    }
    async updateClient(client: Client): Promise<Client> {
        const sql:string = 'update client set first_name=$1, last_name=$2 where primary_key=$3'
        const values = [
            client.fname,
            client.lname,
            client.primaryKey];
        const result = await connect.query(sql,values);
        return client;
    }
    async deleteClientByPrimaryKey(primaryKey: number): Promise<boolean> {
        const sql:string = 'delete from client where primary_key = $1'
        const values = [primaryKey]
        const result = await connect.query(sql,values);
        return true;
    }
}