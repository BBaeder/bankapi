import { Client } from "../entities";
export interface ClientDAO{
    // CREATE
    createClient(client:Client):Promise<Client>;
    // READ
    getAllClients():Promise<Client[]>;
    getClientByPrimaryKey(primaryKey:number):Promise<Client>;
    // UPDATE
    updateClient(client:Client):Promise<Client>;
    // DELETE
    deleteClientByPrimaryKey(primaryKey:number):Promise<boolean>;
}