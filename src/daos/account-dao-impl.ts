import { Account } from "../entities";
import { AccountDAO } from "./account-dao";
import { connect } from "../connection"

export class AccountDAOImpl implements AccountDAO{

    async createAccount(account: Account, foreignKey: number): Promise<Account> {
        const sql:string = 'insert into account(is_checking,amount,foreign_key) values ($1,$2,$3) returning primary_key';
        const values = [
            account.isChecking,
            Number(account.amount),
            foreignKey
        ];
        const result = await connect.query(sql, values);
        account.primaryKey = result.rows[0].primary_key;
        account.foreignKey = foreignKey;
        return account;
    }
    async getAllAccounts(): Promise<Account[]> {
        const sql:string = "select * from account";
        const result = await connect.query(sql);
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
                row.primary_key,
                row.is_checking,
                Number(row.amount),
                row.foreign_key);
            accounts.push(account);
        }
        return accounts;
    }
    async getAccountByPrimaryKey(primaryKey: number): Promise<Account> {
        const sql:string = 'select * from account where primary_key = $1';
        const values = [primaryKey];
        const result = await connect.query(sql, values);
        const row = result.rows[0];
        const account:Account = new Account(
            row.primary_key,
            row.is_checking,
            Number(row.amount),
            row.foreign_key);
        return account;
    }
    async getAllAccountsByForeignKey(foreignKey: number): Promise<Account[]> {
        const sql:string = 'select * from account where foreign_key = $1';
        const values = [foreignKey];
        const result = await connect.query(sql, values);
        const accounts:Account[] = [];
        for(let i = 0; i < result.rowCount;i++){
            const row = result.rows[i];
            const account:Account = new Account(
                row.primary_key,
                row.is_checking,
                Number(row.amount),
                row.foreign_key);
            accounts.push(account);
        }
        return accounts;
    }
    async updateAccount(account: Account): Promise<Account> {
        const sql:string = 'update account set is_checking=$1, amount=$2 where primary_key=$3'
        const values = [
            account.isChecking,
            Number(account.amount),
            account.primaryKey];
        const result = await connect.query(sql,values);
        return account;
    }
    async deleteAccountByPrimaryKey(primaryKey: number): Promise<boolean> {
        const sql:string = 'delete from account where primary_key = $1'
        const values = [primaryKey]
        const result = await connect.query(sql,values);
        return true;
    }
    async deleteAllAccountsByForeignKey(foreignKey: number): Promise<boolean> {
        const sql:string = 'delete from account where foreign_key = $1'
        const values = [foreignKey]
        const result = await connect.query(sql,values);
        return true;
    }
}