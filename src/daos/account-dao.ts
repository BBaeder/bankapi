import { Account, Client } from "../entities";
export interface AccountDAO{
    // CREATE
    createAccount(account:Account,foreignKey:number):Promise<Account>;
    // READ
    getAllAccounts():Promise<Account[]>;
    getAccountByPrimaryKey(primaryKey:number):Promise<Account>;
    getAllAccountsByForeignKey(foreignKey:number):Promise<Account[]>;
    // UPDATE
    updateAccount(account:Account):Promise<Account>;
    // DELETE
    deleteAccountByPrimaryKey(primaryKey:number):Promise<boolean>;
    deleteAllAccountsByForeignKey(foreignKey:number):Promise<boolean>;
}